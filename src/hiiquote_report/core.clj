;; Copyright: 2018, Concitus, Dhaka, Bangladesh.
;; Author: Titon Barua <titon@vimmaniac.com>
(ns hiiquote-report.core
  (:require [cheshire.core :as csh]
            [medley.core :as med]
            [buddy.core.mac :as mac]
            [buddy.core.codecs :as codecs]
            [clojure.pprint :refer [pprint]]
            [clojure.core.reducers :as r]
            [clj-time.core :as time]
            [clj-time.format :as time-fmt]
            [clj-http.client :as http-client]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.set :as set]
            [clojure.string :as str])
  (:gen-class))

(def default-timezone "EST")
(def input-date-format "YYYY-MM-dd")

(def default-timezone-obj
  (time/time-zone-for-id default-timezone))

(defn date-parser
  [val]
  (let [val   (str/lower-case (str/trim val))
        today (-> (time/now)
                  (time/to-time-zone default-timezone-obj)
                  (time/with-time-at-start-of-day))]
    (cond
      (= val "today")
      today,

      (= val "yesterday")
      (time/plus today (time/day -1))

      ;; Accept integers as offset from today.
      (try (Integer/parseInt val)
           (catch NumberFormatException _ nil))
      (time/plus today (time/day (Integer/parseInt val)))

      :else
      (time-fmt/parse (time-fmt/formatter
                       input-date-format
                       default-timezone-obj)
                      val))))

(defn access-parser
  [val]
  (str/split val #":" 2))

(def cli-options
  [[nil
    "--sa-url <subagent-status-api-url>"
    "URL of SubAgentStatus API."
    :default "https://www.hiiquote.com/webservice/getsubagentdetails"]

   [nil
    "--bs-url <business-status-api-url>"
    "URL of BusinessStatus API."
    :default "https://www.hiiquote.com/webservice/getstatus"]

   [nil
    "--ms-url <member-status-api-url>"
    "URL of MemberStatus API."
    :default "https://www.hiiquote.com/webservice/getmemberstatus"]

   [nil
    "--sa-access <access-name>:<access-token>"
    "Access name & code of SubAgentStatus API."
    :required true
    :parse-fn access-parser
    :missing "Missing required option '--sa-access'!"]

   [nil
    "--bs-access <access-name>:<access-token>"
    "Access name & code of BusinessStatus API."
    :parse-fn access-parser
    :missing "Missing required option '--bs-access'!"]

   [nil
    "--ms-access <access-name>:<access-token>"
    "Access name & code of MemberStatus API."
    :parse-fn access-parser
    :missing "Missing required option '--ms-access'!"]

   ["-h"
    "--help"
    "Displays usage information."
    :id :help]

   [nil
    "--start-date <start-date>"
    "Start date. Valid values: today|yesterday|-2,-3,-4...(Offset from today)|2000-08-31(YYYY-MM-DD)"
    :parse-fn date-parser
    :default (date-parser "today")
    :missing "Missing required option '--start-date'!"]

   [nil
    "--end-date <end-date>"
    "End date. Accepts same format as start-date."
    :parse-fn date-parser
    :default (date-parser "today")]

   ["-c"
    "--save-members-to-csv-file <csv-path>"
    "Save member status data to the specified csv file path."
    :id :csv-path
    :default nil]

   ["-p"
    "--parallel-threads <n-threads>"
    "Number of parallel threads used for fetching member status."
    :id :n-threads
    :parse-fn #(Integer/parseInt %)
    :default 10]])

(def debug-http? false)

(defn fetch-api-response
  [url access-name access-code data]
  (let [data-json (csh/generate-string data)
        data-hmac (-> data-json
                      (mac/hash {:key access-code :alg :hmac+sha256})
                      (codecs/bytes->hex))
        headers   {"X-Public"     access-name
                   "X-Hash"       data-hmac
                   "Content-Type" "application/json"}]
    (-> (http-client/post url
                          {:headers headers
                           :debug?  debug-http?
                           :body    data-json
                           :accept  :json})
        (:body)
        (csh/parse-string))))

(def max-sub-agent-records-per-request 1000)

(defn fetch-sub-agents
  ([url access-name access-code]
   (loop [page-no 1
          agents  []]
     (let [new-agents (fetch-sub-agents url access-name access-code page-no)]
       (if (>= (count new-agents) max-sub-agent-records-per-request)
         (recur (inc page-no)
                (concat agents new-agents))
         (concat agents new-agents)))))

  ([url access-name access-code page-no]
   (let [data {"data" [{"Page" (str page-no)}]}]
     (-> (fetch-api-response url access-name access-code data)
         (get-in ["result" 0 "data"])))))

(def max-business-status-records-per-request 100)
(def bs-date-formatter (time-fmt/formatter "yyyy-MM-dd"))

(defn fetch-business-status-records
  ([url access-name access-code start-date end-date]
   (loop [page-no 1
          records []]
     (let [new-records (fetch-business-status-records url
                                                      access-name
                                                      access-code
                                                      start-date
                                                      end-date
                                                      page-no)]
       (if (>= (count new-records) max-business-status-records-per-request)
         (recur (inc page-no)
                (concat records new-records))
         (concat records new-records)))))

  ([url access-name access-code start-date end-date page-no]
   (let [sdate (time-fmt/unparse bs-date-formatter start-date)
         edate (time-fmt/unparse bs-date-formatter end-date)]
     (let [data {"data" [{"Start_Date" sdate
                          "End_Date"   edate
                          "Page"       (str page-no)}]}]
       (-> (fetch-api-response url access-name access-code data)
           (get-in ["result" 0 "data"]))))))

(defn fetch-member-status
  [url access-name access-code member-id user-id]
  (let [data {"data" [{"Member_ID" member-id
                       "User_ID"   user-id}]}]
    (-> (fetch-api-response url access-name access-code data)
        (get-in ["result" "data"]))))

(defn extract-plan
  [record]
  (-> record
      (get "Plan_Details")
      (select-keys ["Plan_ID" "Plan_Name"])))

(defn extract-addon-plans
  [record]
  (as-> record $
    (get $ "Add-on_Plan_Details")
    (map #(select-keys % ["Plan_ID" "Plan_Name"]) $)))

(defn row-from-data-map
  [cols data]
  (str/join "|"
            (for [c cols]
              (str/replace (str (get data c "")) #"\s+" " "))))

(defn print-records-to-csv
  [records]
  (let [cols (sort (set (reduce concat (map keys records))))]
    (str (str/join "|" cols) "\n"
         (->> records
              (map (partial row-from-data-map cols))
              (str/join "\n"))
         "\n")))

(def first-payment-date-format "MM-dd-yyyy")
(defn valid-first-payment-date?
  [start-date end-date record]
  (let [fpd (->> (get record "First_Payment_Date")
                 (time-fmt/parse (time-fmt/formatter
                                  first-payment-date-format
                                  default-timezone-obj)))]
    (time/within? start-date end-date fpd)))

(defn run
  [{:as   options
    :keys [sa-url
           bs-url
           ms-url
           sa-access
           bs-access
           ms-access
           start-date
           end-date
           csv-path
           n-threads]}]
  (println (str "Running with options:\n"
                (with-out-str (pprint options))))
  (let [-                     (println "Fetching sub-agent records ...")
        sa-records            (future (apply fetch-sub-agents sa-url sa-access))
        -                     (println "Fetching business status records ...")
        bs-records            (future (apply fetch-business-status-records
                                             bs-url
                                             (concat bs-access
                                                     [start-date
                                                      end-date])))
        joined-records        (->> (set/join (set @sa-records)
                                             (set @bs-records)
                                             {"Agent Code" "Agent Code"})
                                   (filter (partial valid-first-payment-date? start-date end-date))
                                   (filter #(= (get % "Payment Status") "Paid"))
                                   (filter #(= (get % "Is_Active_Enrollment" "True"))))
        unique-records        (med/distinct-by #(select-keys % ["Member ID" "User ID"])
                                               joined-records)
        _                     (printf "Fetching %d unique member status records ...%n"
                                      (count unique-records))
        member-status-records (r/fold n-threads
                                      concat
                                      conj
                                      (r/map #(->> (apply fetch-member-status
                                                          ms-url
                                                          (concat ms-access
                                                                  [(% "Member ID")
                                                                   (% "User ID")]))
                                                   (merge %))
                                             unique-records))
        plans                 (->> member-status-records
                                   (filter #(= (% "Is_Main_Plan") "True"))
                                   (map extract-plan))
        addon-plans           (reduce concat (map extract-addon-plans member-status-records))]
    (when csv-path
      (spit csv-path (print-records-to-csv member-status-records)))
    (printf "Plan counts:%n%s"
            (with-out-str (pprint (frequencies plans))))
    (printf "Addon-plan counts:%n%s"
            (with-out-str (pprint (frequencies addon-plans))))))

(defn print-usage
  [summary errors]
  (printf "Usage: 'lein run [OPTIONS]' OR 'java -jar program.jar [OPTIONS]'%s%n%nOptions summary:%n%s%n"
          (if errors
            (str "\n\nErrors:\n" (str/join "\n" errors))
            "")
          summary)
  (flush))

(defn -main
  [& args]
  (let [{:keys [options
                summary
                errors]} (parse-opts args cli-options)]
    (cond
      (:help options)
      (do (print-usage summary nil)
          (System/exit 0))

      errors
      (do (print-usage summary errors)
          (System/exit 1))

      :else
      (do (run options)
          (flush)
          (System/exit 0)))))
