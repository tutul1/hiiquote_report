# Hiiquote Daily sales report tool
From command line you can request daily successful sales submitted by an Hiiquote agent and their downline agents directly from their API.
If ran in a peridic task, this tool can email the sales count to the recipient, or submit the data to a dashboard.
## Installation

- Install clojure 1.9.
- Install lein.
- Run with `lein run`.

## Usage:

Usage information is available with `lein run -- -h`.


Usage: 'lein run [OPTIONS]' OR 'java -jar program.jar [OPTIONS]'

Options summary:
```
--sa-url <subagent-status-api-url>        https://www.hiiquote.com/webservice/getsubagentdetails  URL of Hiiquote SubAgentStatus API.
--bs-url <business-status-api-url>        https://www.hiiquote.com/webservice/getstatus           URL of Hiiquote BusinessStatus API.
--ms-url <member-status-api-url>          https://www.hiiquote.com/webservice/getmemberstatus     URL of Hiiquote MemberStatus API.
--sa-access true                                                                                  Access name & code of Hiiquote SubAgentStatus API.
--bs-access <access-name>:<access-token>                                                          Access name & code of Hiiquote BusinessStatus API.
--ms-access <access-name>:<access-token>                                                          Access name & code of Hiiquote MemberStatus API.
-h, --help                                                                                            Displays usage information.
--start-date <start-date>                 2018-10-09T00:00:00.000-05:00                           Start date. Valid values: today|yesterday|-2,-3,-4...(Offset from today)|2000-08-31(YYYY-MM-DD)
--end-date <end-date>                     2018-10-09T00:00:00.000-05:00                           End date. Accepts same format as start-date.
-c, --save-members-to-csv-file <csv-path>     nil                                                     Save Hiiquote member status data to the specified csv file path.
-p, --parallel-threads <n-threads>            10                                                      Number of parallel threads used for fetching Hiiquote member status.
```

## License
GPLv3

Copyright � 2018 Concitus, Dhaka, Bangladesh.
